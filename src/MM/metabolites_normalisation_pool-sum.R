
library(tidyverse)
library(stringr)
library(here)


#####global paths
# global paths
source(here("conf","config.R"))
DATA=paste(RESULTSDIR,"Databases/Metabolomics/Raw_Tables",sep="/") 
# OUTDIR=paste(RESULTSDIR,"Databases/Metabolomics/Normalised_Tables",sep="/")


#### Load Data ####
samples=read.table(paste(REPODIR,"aux_datafiles","sample_list.txt",sep="/"))
colnames(samples)=c("sample","date")

bnp.mets=read.csv(paste(DATA,"20140418_EM_BNP_raw.csv",sep="/"),sep="\t",encoding = "UTF-8")
snp.mets=read.csv(paste(DATA,"20140418_EM_SNP_raw.csv",sep="/"),sep="\t",encoding = "UTF-8")
bp.mets=read.csv(paste(DATA,"20140422_CJ_BP_raw.csv",sep="/"),sep="\t",encoding = "UTF-8")
sp.mets=read.csv(paste(DATA,"20140422_CJ_SP_raw.csv",sep="/"),sep="\t",encoding = "UTF-8")
###Fix sample IDs to match sample IDs in metabolite tables
samples[,1]=gsub("D0([1-9])","D\\1",samples[,1])
colnames(samples)[1]="sample"

##fix samples IDs to match to sequences
colnames(bnp.mets)=gsub("\\.cmp$","",colnames(bnp.mets))
colnames(snp.mets)=gsub("\\.cmp$","",colnames(snp.mets))
colnames(bp.mets)=gsub("\\.cmp$","",colnames(bp.mets))
colnames(sp.mets)=gsub("\\.cmp$","",colnames(sp.mets))

##load sequences
#
bnp.seq=read_tsv(paste(DATA,"20161102_CJ_EM_waste_water_sequence_BNP.csv",sep="/"),col_names = F)
snp.seq=read_tsv(paste(DATA,"20161102_CJ_EM_waste_water_sequence_SNP.csv",sep="/"),col_names = F)
bp.seq=read_tsv(paste(DATA,"20161102_CJ_EM_waste_water_sequence_BP.csv",sep="/"),col_names = F)
sp.seq=read_tsv(paste(DATA,"20161102_CJ_EM_waste_water_sequence_SP.csv",sep="/"),col_names = F)

#fix sequences 
bnp.seq$X5=gsub("__","_",bnp.seq$X5)
snp.seq$X5=gsub("__","_",snp.seq$X5)
sp.seq$X5=gsub("__","_",sp.seq$X5)
bp.seq$X5=gsub("__","_",bp.seq$X5)

#sp Slak Blak -> Blank
sp.seq$X5=gsub("Slak|Blak","Blank",sp.seq$X5)
#sp.seq missing some samples taking snp.seq instead
sp.seq=snp.seq
sp.seq$X5=gsub("SNP","SP",sp.seq$X5)

#replace bnp.seq with snp.seq rename samples in X5
bnp.seq=snp.seq
bnp.seq$X5=gsub("SNP","BNP",bnp.seq$X5)

#remove metabolites with 0 intensity in any pool or <25% non-zero values across samples
filter_step1 <- function(tab){
  #100% nonzeros in pool samples
  tab %>% select(Metabolite,contains("Pool")) %>%
    gather(pool_num,value,-Metabolite) %>%
    group_by(Metabolite) %>%
    summarise(pools_nonzero=length(value[which(value!=0)])/length(value)) %>%
    filter(pools_nonzero==1) %>%
    pull(Metabolite) -> metlist.nonzero.pools
  #at least 90% non zero intensities across sampples TODO , replace 3 of 3 replicates not-zero
  tab %>% select(Metabolite,matches("_D\\d+_")) %>%
    gather(sample,value,-Metabolite) %>%
    group_by(Metabolite) %>%
    summarise(samples_nonzero=length(value[which(value!=0)])/length(value)) %>%
    filter(samples_nonzero>=0.25) %>%
    pull(Metabolite) -> metlist.nonzero.samples
  metlist.nonzero = intersect(metlist.nonzero.pools,metlist.nonzero.samples)
  tab %>% select(Metabolite,contains("Blank"),contains("Pool"),matches("_D\\d+_")) %>%
    # filter(!Metabolite=="!13C_ribitol_5TMS")%>%
    filter(Metabolite %in% metlist.nonzero) -> mettab.nonzero
  return(mettab.nonzero)
}

poolnorm_step3 <- function(tab2,seq){
  #extract seq
  sequence=data.frame(num=as.integer(rownames(seq)),sample=seq$X5)
  
  #check if all sampleIDs are also in the sequence file and vice versa
  print("sample IDs found in data table but not in measurement sequence:")
  aa1=colnames(tab2)[which(!(colnames(tab2)[-1] %in% sequence$sample)) ]
  print(aa1)
  if(length(aa1)>=1){
    break()
  }
  print("sample IDs found in sequence but not in data table:")
  print(sequence$sample[which(!(sequence$sample %in% colnames(tab2)[-1] )) ])
  
  sequence %>% filter(str_detect(sample,"Pool")) -> pools
  sequence %>% filter(!str_detect(sample,"Pool")) -> seqs
  #get pools sourrounding sample num 1 prior 1 post
  seqs %>%
    mutate(pools_sour= map(num,function(x){
      pools %>%
        mutate(minus=pools$num-x) %>%
        arrange(abs(minus)) %>%
        filter(abs(minus)<=6) %>%
        pull(sample) %>% as.character %>% .[1:2] 
    })) -> samples_pools
  
  #normalize input table by pools
  #as.tibble so pool_values stay a vector if there is one pool missing
  tab2.norm=tab2%>%as.tibble()
  for (cname in colnames(tab2.norm)){
    if (!(cname %in% samples_pools$sample)){
      if (!grepl("_Pool_",cname)){
        print(paste("column:<",cname,"> not in sample sequence, skipping"))
      }
      next()
    }
    samples_pools %>%
      filter(sample==cname) %>% pull(pools_sour) -> pool_names
    pool_values <- tab2.norm[,which(colnames(tab2.norm) %in% pool_names[[1]])]
    if(dim(pool_values)[2]<2){
      print(paste("not 2 sourrounding pools available for",cname,"using",colnames(pool_values)))
    }
    pool_means <- rowMeans(pool_values)
    tab2.norm[,which(colnames(tab2.norm)==cname)] <- tab2.norm[,which(colnames(tab2.norm)==cname)] /pool_means
  }
  tab2.norm %>% select(-contains("Pool")) -> outtab
  final=outtab
  return(final)
}



#filter out metabolites that have higher mean values in blanks than in samples, (blanks are derivatization blanks)
#which have blank value means are at least 0.75 times or more higher than sample means
filter_blanks_step2 <- function(tab2){
  #filter on blanks before or after pool norm??
  #check if blanks are higher than values in samples
  tab2 %>% select(Metabolite,contains("Blank")) -> blanks
  tab2 %>% select(Metabolite,matches("_D\\d+_")) -> samplecols
  remove_idx =c()
  for (i in 1:length(tab2$Metabolite)){
    mean_samples = rowMeans(samplecols[i,-1])
    mean_blanks = rowMeans(blanks[i,-1])
    max_blanks = max(blanks[i,-1])
    if (mean_blanks > 0.75*mean_samples){
      print(paste("check blanks for",tab2$Metabolite[i],"mean value in blanks",
                  mean_blanks,"higher than mean value of samples,",mean_samples,"removing metabolite"))
      remove_idx=c(remove_idx,i)
    }
    # if (max_blanks > mean_samples){
    #   print(paste("check blanks for",tab2$Metabolite[i],"max value in blanks",
    #               max_blanks,"higher than mean value of samples,",mean_samples,"removing metabolite"))
    # }
  }
  if (length(remove_idx)>1){
    xx1<-tab2[-remove_idx,]
  }else{
    xx1 <- tab2
  }
  return(xx1)
}


gather_nmatch <- function(tab_filtered, tab_pool, samples,typestr) {
  tab_filtered = tab_filtered %>% select(Metabolite,matches("_D\\d+_"))
  tab.gat <- tab_filtered %>% gather(replicate, intensity, -Metabolite) %>%
    mutate(sample = gsub(".+_(D\\d+)_.+", "\\1", replicate)) %>% 
    left_join(samples)
  pool.gat <- tab_pool %>% 
    select(Metabolite,matches("_D\\d+_")) %>%
    gather(replicate, intensity.poolnorm, -Metabolite) %>%
    mutate(sample = gsub(".+_(D\\d+)_.+", "\\1", replicate)) %>% 
    left_join(samples)
  merged <- tab.gat %>% left_join(pool.gat) %>%
    group_by(Metabolite, sample, date) %>%
    summarise(intensity.median = median(intensity), intensity.poolnorm.median = median(intensity.poolnorm)) %>%
    mutate(type=typestr)
  return(merged)  
}

#filter
bnp.filt <- filter_step1(bnp.mets)
snp.filt <- filter_step1(snp.mets)
bp.filt <- filter_step1(bp.mets)
sp.filt <- filter_step1(sp.mets)

#bnp.blanks <- filter_blanks_step2(bnp.filt)
#snp.blanks <- filter_blanks_step2(snp.filt)
#bp.blanks <- filter_blanks_step2(bp.filt)
#sp.blanks <- filter_blanks_step2(sp.filt)

#pool normalisation
bnp.pool <- poolnorm_step3(bnp.filt,bnp.seq)
snp.pool <- poolnorm_step3(snp.filt,snp.seq)
bp.pool <- poolnorm_step3(bp.filt,bp.seq)
sp.pool <- poolnorm_step3(sp.filt,sp.seq)

#filter out when values in blanks are too high
bnp.pool.blanks <- filter_blanks_step2(bnp.pool)
snp.pool.blanks <- filter_blanks_step2(snp.pool)
bp.pool.blanks <- filter_blanks_step2(bp.pool)
sp.pool.blanks <- filter_blanks_step2(sp.pool)


bnp.m <- gather_nmatch(bnp.filt, bnp.pool.blanks, samples,"bnp")
snp.m <- gather_nmatch(snp.filt, snp.pool.blanks, samples,"snp")
bp.m <- gather_nmatch(bp.filt, bp.pool.blanks, samples,"bp")
sp.m <-  gather_nmatch(sp.filt, sp.pool.blanks, samples,"sp")
merged <- bind_rows(bnp.m, snp.m, bp.m, sp.m )

### wrigte out 
saveRDS(merged, file= paste(RESULTSDIR,"Output/Metabolite_data_poolnorm.RDS",sep="/"))

## get values per metabolite
## match kegg IDs
match_keggids<-function(df,dict){
  dict=dict[,c(1,3)]
  tt=aggregate(KEGG.Compound.ID ~ X, data=dict, paste, collapse=",")
  tt$KEGG.Compound.ID=gsub("\\n|^\\s|\\s$","",tt$KEGG.Compound.ID)
  ss=left_join(df,tt,by=c("Metabolite"="X"))
  ff=as.data.frame(append(df, list(ss$KEGG.Compound.ID), after = 1))
  colnames(ff)[2]="KEGG.Compound.ID"
  return(ff)
}


###
met_conversion=read.csv(paste(REPODIR,"aux_datafiles","all_ids_updated_CJ.csv",sep="/"),fill=T,sep="\t",header=T,encoding = "UTF-8")

met_conversion$KEGG.Compound.ID=gsub("\\n|^\\s|\\s$","",met_conversion$KEGG.Compound.ID)
met_conversion$Chebi.Name=gsub("\\n|^\\s|\\s$","",met_conversion$Chebi.Name)

qq <- match_keggids(merged, met_conversion)

dict=met_conversion[,c(1,3,6)]

tt=aggregate(KEGG.Compound.ID ~ X, data=dict, paste, collapse=",")
ff=aggregate(Chebi.Name ~ X, data=dict, paste, collapse=";")

#deredundify chebi names
z=sapply(ff$Chebi.Name, function(x) paste(unique(str_split(x,";")[[1]]),collapse=";"))
ff$Chebi.Name_combined=z
#add Chebi.Name_combined to m.p and m.np
zz=left_join(tt,ff)
colnames(zz)[1]="Metabolite"

fin <- qq %>% left_join(zz) %>% select(-Chebi.Name) %>% 
  mutate(met_name = ifelse(is.na(Chebi.Name_combined) | Chebi.Name_combined =="", Metabolite, Chebi.Name_combined)) %>%
  mutate(type2=ifelse(grepl("n",type), "non-polar","polar"),
         type3=ifelse(grepl("s",type), "extracellular","intracellular"))

##filter out unknowns and
## summarise multiple derivates by merging on met_name (take max intensity)

filt.summarised <- fin %>%
  filter(!grepl("No direct match|No match|unknown", Metabolite, ignore.case=T)) %>% ##filter out unknowns
  filter(!grepl("^!", Metabolite)) %>%
  filter(!is.na(intensity.poolnorm.median)) %>%
  group_by(KEGG.Compound.ID, sample, date, type, Chebi.Name_combined, met_name, type2, type3) %>%
  summarise(intensity.poolnorm.median.max = max(intensity.poolnorm.median)) %>%
  ungroup()

##check for metabolites appearing both in polar and non-polar
multipolar <- filt.summarised %>% select(met_name, type2, type3) %>% distinct() %>%
  group_by(met_name, type3) %>% summarise(appearances = length(type2)) %>%
  arrange(desc(appearances)) %>%
  filter(appearances>=2)

#filter out nonpolar metabolites appearing in polar fraction
filt.summarised <- filt.summarised %>%
  filter(!(met_name %in% multipolar$met_name & type2=="polar"))

merged.sumnorm <- filt.summarised %>%
  # filter(!grepl("No direct match|No match|unknown", Metabolite, ignore.case=T)) %>% ##filter out unknowns
  # filter(!grepl("^!", Metabolite)) %>%
  # filter(!is.na(intensity.poolnorm.median)) %>%
  group_by(sample, date, type, type2, type3) %>%
  mutate(intensity.sumnorm = intensity.poolnorm.median.max/sum(intensity.poolnorm.median.max, na.rm=T))

##test
merged.sumnorm %>%
  group_by(sample,date,type) %>%
  summarise(ss = sum(intensity.sumnorm))


###
saveRDS(merged.sumnorm, file= paste(RESULTSDIR,"Output/Metabolite_data_normalised.RDS",sep="/"))





