### correlation of metabolites and physico chemical parameters to MG and MT shifts

library(data.table)
library(stringr)
library(tidyverse)
library(stringr)
library(here)
library(RColorBrewer)
library(gridExtra)
library(psych)
library(broom)


#####global paths
# global paths
#setwd("/home/mh/Uni_Lux/REPOS/2019_Niche_Ecology_LAO-TS/")
source(here("conf","config.R"))

##samplelist
samples=read.table(paste(REPODIR,"aux_datafiles","sample_list.txt",sep="/"), col.names = c("sampleID","date"))
# here::here doesnt work anymore after loading lubridate
library(lubridate)
# samples$month = month(samples$date)
# samples$year= year(samples$date)
samples<-samples %>% rename(Date=date) %>%
  mutate(Date=as.Date(Date)) %>%
  filter(Date > "2011-03-01")


#### read in metabolites ####
# mets <- readRDS(paste(RESULTSDIR,"Output/Metabolite_data_normalised.RDS",sep="/")) %>% ungroup()
# mm <- mets %>% mutate(variable = paste(met_name,type,sep="_")) %>%
#   select(variable, Date=date, intensity.sumnorm)
# mm1 <- mm %>% spread(variable, intensity.sumnorm)
# mm2 = apply(mm1[-1], 2, diff)
# mets.diff <- tibble(Date=mm1$Date[-1]) %>% cbind(mm2) %>%
#   mutate(Date=as.Date(Date))
# 
# met.anno = mets %>% mutate(variable = paste(met_name,type,sep="_")) %>%select(variable, met_name, type, type2, type3) %>% distinct()

#### pcparams ####
# params_online <- readRDS(paste(RESULTSDIR,"Output/pcparams_daily-avgs_2008-2018.RDS",sep="/"))
# params_manual <- read_tsv(paste0(RESULTSDIR,"/Databases/PhysicoChemical/pc_params.manual.processed_sampling_dates.tsv"))
# params <- bind_rows(params_online,
#                     params_manual %>% gather(variable, daily_avg, -Date) %>% mutate(type="manual"))
# params.sampling <- left_join(samples, params)
# pp1 <- params.sampling %>%
#   select(Date, variable, daily_avg) %>%
#   spread(variable, daily_avg)
# pp2 <- apply(pp1[-1],2,diff)
# pp.diff <- tibble(Date=pp1$Date[-1]) %>% cbind(pp2)

# pp.anno <- params.sampling %>% select(variable, unit, type) %>% distinct()
##combined with mets
# abiotic <- left_join(mets.diff, pp.diff)

  
####MT, MG, relative abundances ####
ALL.mg.input <- paste(sep="/",RESULTSDIR,"PopulationAnalysis//Calculations/PopulationLevel/ALL.mg.average_population_depth.txt")
ALL.mt.input <- paste(sep="/", RESULTSDIR,"PopulationAnalysis//Calculations/PopulationLevel/ALL.mt.average_population_depth.txt")

reGes.input <- paste(REPODIR,"aux_datafiles","representative_bins.filtered.tax.tsv",sep="/")
mg.pop <- read_tsv(ALL.mg.input, col_names = T)
mt.pop <- read_tsv(ALL.mt.input, col_names = T) %>% select(-length)
reGes <- read_tsv(reGes.input, col_names = "population")

#load samples
samplelist=paste(REPODIR,"aux_datafiles/sample_list.txt",sep="/")
samples=read.table(samplelist,sep="\t")
colnames(samples)=c("sampleID","date")

###
mg.pop %>%
  filter(population %in% reGes$population) %>% 
  gather(date, depth, -population) %>% 
  mutate(date = as.Date(date)) %>% 
  filter(!grepl("2010-10-04", date)) %>% 
  filter(!grepl("2011-01-25", date)) %>%
  group_by(date) %>%
  mutate(sum_depth = sum(depth)) %>%
  group_by(population, add=TRUE) %>%
  mutate(proportion = 100*depth/sum_depth) -> mg.rel

mt.pop %>%
  filter(population %in% reGes$population) %>% 
  gather(date, depth, -population) %>% 
  mutate(date = as.Date(date)) %>% 
  filter(!grepl("2010-10-04", date)) %>% 
  filter(!grepl("2011-01-25", date)) %>%
  group_by(date) %>%
  mutate(sum_depth = sum(depth)) %>%
  group_by(population, add=TRUE) %>%
  mutate(proportion = 100*depth/sum_depth) -> mt.rel


# # ## load zscore normalized parameters
# outparams=paste(RESULTSDIR,"params_zscore.rds",sep="/")
# sp_pc.zscore=readRDS(outparams)
# metsmat <- readRDS(paste(RESULTSDIR,"Output/figures_rds/sfig_4_mets-heatmap.list.rds",sep="/"))[[1]]
# metsanno <- readRDS(paste(RESULTSDIR,"Output/figures_rds/sfig_4_mets-heatmap.list.rds",sep="/"))
mets <- readRDS(paste(RESULTSDIR, "Output", "pcparams.zscores.withratios.new.rds", sep="/"))

inmets <- mets %>% 
  filter(retain=="yes") %>%
  mutate(measforparam = ifelse(measurement=="pcparam","",measurement)) %>%
  mutate(param= paste(corrected_name, measforparam, sep="_"))

sp_pc.zscore <- inmets %>%
  select(param, zscore, Date) %>%
  spread(param, zscore) %>%
  column_to_rownames("Date")



############## Differences

diff_mets <- apply(sp_pc.zscore, 2, diff )  %>% as.data.frame()
abiotic = diff_mets

##diff mg
mg.rel_spread <- mg.rel %>% select(date,proportion,population) %>%
  spread(population, proportion) %>% as.data.frame()
rownames(mg.rel_spread) = mg.rel_spread$date
diff_mg <- apply(mg.rel_spread[-1], 2, diff )
diff_mg = as.data.frame(diff_mg)

##diff mt
mt.rel_spread <- mt.rel %>% select(date,proportion,population) %>%
  spread(population, proportion) %>% as.data.frame()
rownames(mt.rel_spread) = mt.rel_spread$date
diff_mt <- apply(mt.rel_spread[-1], 2, diff )
diff_mt = as.data.frame(diff_mt)


#### 


#### get correlations ####
##cor mat
get_cor <- function(x, y, adjust, method) {
  corrrmat <- corr.test(x, y, ci=F, adjust=adjust, method=method)
  pvals <- cbind(tibble(variable = rownames(corrrmat$p)), corrrmat$p)
  ff <- pvals %>% gather(binID, pval, -variable) %>%
    arrange(pval)
  
  corrvals <- cbind(tibble(variable = rownames(corrrmat$r)), corrrmat$r) %>% gather(binID, rval, -variable)
  ff <- left_join(ff, corrvals)
  return(ff)
}


### subset abiotic 
# abiotic.subs <- abiotic %>%
  # select(Date, matches("glycer|octa|hexad|nitrate|outresc|ethanolam|ose$"))


# mt.diff <- get_cor(abiotic[-1], diff_mt, "fdr", "pearson")
mg.diff <- get_cor(abiotic, diff_mg, "fdr", "pearson")

# mt.diff <- get_cor(abiotic.subs[-1], diff_mt, "none", "pearson")
# mg.diff <- get_cor(abiotic.subs[-1], diff_mg, "none", "pearson")

# mt.diff %>%
#   # filter(binID=="A01_O1.2.4") %>%
#   filter(binID=="D51_G1.1.2") %>%
#   # filter(pval < 0.05 ) %>%
#   arrange(pval) %>% View

mg.diff %>%
  # filter(pval < 0.05) %>%
  arrange(pval) %>% View


## add annotation
## annotation
anno.f <- readRDS(paste(RESULTSDIR,"Output/Reges_annotations_genesfuncstax.RDS", sep="/")) %>%
  mutate(KO=gsub("ko:","",KO))
anno <- anno.f %>% select(binID, FunC_ID, phylum, class, order, family, genus, species) %>% distinct()


metsanno <- inmets %>% 
  select(param, type, fraction, measurement, metabolite_class) %>%
  distinct()
# ab.anno <- bind_rows(met.anno, pp.anno)
# xx <- mg.diff %>% left_join(ab.anno)
# 
# xx %>% 
#   filter(!type %in% c("snp","bnp","sp","bp"))

### try heatmap
library(pheatmap)

col.anno <- anno %>% select(binID, phylum, FunC_ID, class) %>%
  filter(!is.na(phylum)) %>% as.data.frame()
rownames(col.anno) = col.anno$binID
col.anno$FunC_ID = as.factor(col.anno$FunC_ID)

mg.diff %>%
  # filter(!grepl("BB2|bnp$|bp$|sp$", variable, ignore.case=T)) %>%
  select(-pval) %>%
  spread(binID, rval) -> mgmat

rownames(mgmat) = mgmat$variable

row.anno = metsanno %>%
  mutate(measurement = as.factor(measurement)) %>%
  mutate(fraction = as.factor(fraction)) %>%
  mutate(metabolite_class = as.factor(metabolite_class)) %>%
  column_to_rownames("param")

mtype=brewer.pal(length(levels(row.anno$measurement)),"Spectral")
names(mtype)=levels(row.anno$measurement)

mclass=brewer.pal(length(levels(row.anno$fraction)),"Set3")
names(mclass)=levels(row.anno$fraction)

met_c = colorRampPalette(brewer.pal(9,"Set1"))(length(levels(row.anno$metabolite_class)))
names(met_c)=levels(row.anno$metabolite_class)

## col colours
##colors for annotations
FunC_col = brewer.pal(4,"Set1")
names(FunC_col)=levels(col.anno$FunC_ID)

phylum_col=colorRampPalette(brewer.pal(8,"Set2"))(length(unique(col.anno$phylum)))
names(phylum_col)=unique(col.anno$phylum)

#class_col=colorRampPalette(brewer.pal(11,"Set3"))(length(unique(mat_col$class)))
class_col=c(
  "#7a67d4", #2
  "#7190cc", #7
  "#cd9430", #8
  "#ce5335", #1
  "#c46762", #11
  "#bd5bb6", #4
  "#d8406d", #6
  "#78ac3d", #3 
  "#4eab7d", #5
  "#98874a", #10
  "#bf6392" #9
)
names(class_col)=levels(as.factor(as.character(col.anno$class)))

## anno colours
ann_colors = list(
  measurement = mtype,
  fraction = mclass ,
  metabolite_class = met_c,
  FunC_ID = FunC_col,
  phylum = phylum_col,
  class = class_col 
)

##heatmap
steps=c(-1,seq(-0.999,0.999,by=0.001),1)

saveRDS(list(mgmat,row.anno,col.anno,ann_colors),paste(RESULTSDIR,"Output","figures_rds","s-fig5_params_mgcor_differenced_pearson",sep="/"))

outpdf = paste(OUTPLOTDIR,"s-fig5_params_mgcor_differenced_pearson.pdf",sep="/")
pdf(outpdf,width=14,height=14)
pheatmap(mgmat[-1],
       breaks = steps,
       color = colorRampPalette(rev(brewer.pal(n = 11, name = "RdYlBu")))(length(steps)),
       border_color = "darkgrey",
       cluster_rows = T,
       cluster_cols = T,
       annotation_col = col.anno[-1],
       annotation_row = row.anno,
       annotation_colors = ann_colors,
       fontsize_row =6,
       fontsize_col = 9
        )
dev.off()



### plot for non differenced

abiotic <- inmets %>%
  select(param, zscore, Date) %>%
  spread(param, zscore) %>%
  column_to_rownames("Date")

mg <- mg.rel %>% 
  select(binID=population, Date=date, proportion) %>%
  spread(binID, proportion) %>%
  column_to_rownames("Date")

mg.cors <- get_cor(abiotic, mg, "fdr", "spearman")

mg.cors %>%
  select(-pval) %>%
  spread(binID, rval) %>%
  column_to_rownames("variable")-> mgmat

saveRDS(list(mgmat,row.anno,col.anno,ann_colors),paste(RESULTSDIR,"Output","figures_rds","s-fig5_params_mgcor_zscores_spearman.rds",sep="/"))

outpdf = paste(OUTPLOTDIR,"s-fig5_params_mgcor_zscores_spearman.pdf",sep="/")
pdf(outpdf,width=14,height=14)
pheatmap(mgmat,
         breaks = steps,
         color = colorRampPalette(rev(brewer.pal(n = 11, name = "RdYlBu")))(length(steps)),
         border_color = "darkgrey",
         cluster_rows = T,
         cluster_cols = T,
         annotation_col = col.anno[-1],
         annotation_row = row.anno,
         annotation_colors = ann_colors,
         fontsize_row =6,
         fontsize_col = 9
)
dev.off()


