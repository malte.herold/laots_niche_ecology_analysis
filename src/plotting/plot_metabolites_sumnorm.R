library(tidyverse)
library(stringr)
library(here)
library(RColorBrewer)
library(gridExtra)



#####global paths
# global paths
source(here("conf","config.R"))


#read in metdata
mets <- readRDS(paste(RESULTSDIR,"Output/Metabolite_data_normalised.RDS",sep="/"))

# read in samples table
samples=read.table(paste(REPODIR,"aux_datafiles","sample_list.txt",sep="/"))[-c(1,2),]
colnames(samples)=c("sample","date")

library(lubridate)
samples$month = month(samples$date)
samples$year= year(samples$date)

getSeason <- function(input.date){
  numeric.date <- 100*month(input.date)+day(input.date)
  ## input Seasons upper limits in the form MMDD in the "break =" option:
  cuts <- base::cut(numeric.date, breaks = c(0,319,0620,0921,1220,1231)) 
  # rename the resulting groups (could've been done within cut(...levels=) if "Winter" wasn't double
  levels(cuts) <- c("Winter","Spring","Summer","Fall","Winter")
  return(cuts)
}

samples$season = getSeason(samples$date)

## group on name 

# dat <- mets %>% 
#   filter(!is.na(intensity.sumnorm)) %>% 
#   group_by(date, met_name, type) %>%
#   summarise(max_intensity = max(intensity.sumnorm)) %>%
#   mutate(type2=ifelse(grepl("n",type), "non-polar","polar"),
#          type3=ifelse(grepl("s",type), "extracellular","intracellular"))

dat=mets


set.seed(1)
cols <- colorRampPalette(brewer.pal(12, "Set3"))(length(unique(dat$met_name)))
cols <- sample(cols)
names(cols) = unique(dat$met_name)

dat.f <- dat %>% ungroup() %>%
  filter(grepl("fructose|lactose|glucose|mannose|octad|hexad|glyc", met_name, ignore.case=T) )
# dat.f = dat
dats <- split(dat.f , f = dat.f$type)


p1 <- dats$bnp %>%
  ggplot(aes (x = as.Date(date), y = intensity.sumnorm, fill = met_name)) + 
  # geom_bar(stat="identity", color="black", size=0.4) +
  # geom_point() + geom_line(aes(color=met_name)) +
  geom_area(color="black", alpha=0.7)+
  facet_grid(type3~type2) +
  scale_fill_manual(values=cols) +
  scale_x_date(breaks=as.Date(samples$date),minor_breaks=NULL,date_labels ="%d-%m-%Y") +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))

p2 <- p1 %+% dats$bp
p3 <- p1 %+% dats$sp
p4 <- p1 %+% dats$snp

grid.arrange(p2,p1,p3,p4, ncol=2,nrow=2) ##### subselection for polar metabolites needed


### visualise changes
dat.diff <- dat %>%
  ungroup() %>%
  select(-sample, -Chebi.Name_combined, -intensity.poolnorm.median.max, -KEGG.Compound.ID) %>%
  mutate(type=as.character(type)) %>% ##nest weird behavior on fct 
  group_by(met_name, type, type2, type3) %>%
  tidyr::nest() 

dat.diff <- dat.diff %>%
  mutate(differenced_data = map(data, function(x) {
    x <- x %>% arrange(date)
    yy <- data.frame(date=x$date[c(2:51)],
        diff.intensity = diff(x$intensity.sumnorm)) 
    return(yy)}))

dat.diff %>%
  select(met_name, type, type2, type3, differenced_data) %>%
  unnest(differenced_data) %>%
  left_join(samples) -> dat.dd

dat.ddf <- dat.dd #%>% filter(abs(diff.intensity) > 0.025) 
dats.dd <- split(dat.ddf, f=dat.ddf$type)
p1 <- dats.dd$bnp %>%
  # filter(abs(diff.intensity) > 0.05) %>%
  ggplot(aes (x = as.Date(date), y = diff.intensity, fill = met_name)) + 
  geom_bar(stat="identity", color="black", size=0.4) +
# geom_point() + geom_line(aes(color=met_name)) +
  # geom_area(color="black", alpha=0.7)+
  facet_grid(type3~type2) +
  scale_fill_manual(values=cols) +
  scale_x_date(breaks=as.Date(samples$date),minor_breaks=NULL,date_labels ="%d-%m-%Y") +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))

p2 <- p1 %+% dats.dd$snp
p3 <- p1 %+% dats.dd$bp
p4 <- p1 %+% dats.dd$sp


grid.arrange(p1,p2, ncol=1,nrow=2) ##### subselection for polar metabolites needed
grid.arrange(p3,p4)  



dat.dd %>%
  filter(grepl("ose$|octa|glycerol|hexad",met_name)) %>%
  # filter(abs(diff.intensity) > 0.05) %>%
  ggplot(aes (x = as.Date(date), y = diff.intensity, fill = met_name)) + 
  geom_bar(stat="identity", color="black", size=0.4) +
  # geom_point() + geom_line(aes(color=met_name)) +
  # geom_area(color="black", alpha=0.7)+
  facet_grid(type3~type2, scale="free") +
  scale_fill_manual(values=cols) +
  scale_x_date(breaks=as.Date(samples$date),minor_breaks=NULL,date_labels ="%d-%m-%Y") +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))


### tops
samples <- samples %>% mutate(season_year = paste(season,year,sep=":")) %>%
  mutate(season_year = as.factor(season_year)) %>% # pull(season_year) %>% levels()
  mutate(season_year = fct_reorder(season_year, as.Date(date)))


dat.dd %>%
  left_join(samples) %>%
  # mutate(season_year = paste(season,year,sep=":")) %>%
  group_by(season_year, met_name, type, type2, type3) %>%
  summarise(mean_intensity_changes = mean(diff.intensity)) %>%
  arrange(desc(abs(mean_intensity_changes))) %>%
  ungroup()  -> dat.season 

dat.season %>%  
# top_n(2, abs(mean_intensity_changes)) %>% arrange(met_name)
  ggplot(aes(x=season_year, y=met_name, color=ifelse(mean_intensity_changes>0, "pos","neg"), 
             size=abs(mean_intensity_changes))) +
  geom_point() +
  facet_grid(type2~type3, scale="free")

##filter
dat.season %>%
  group_by(met_name, type2) %>%
  summarise(means = mean(abs(mean_intensity_changes))) %>%
  ungroup() %>%
  group_by(type2) %>%
  top_n(20,means) -> dat.season.filt 

dat.season %>%  
  filter(met_name %in% dat.season.filt$met_name) %>%
  # top_n(2, abs(mean_intensity_changes)) %>% arrange(met_name)
  ggplot(aes(x=season_year, y=met_name, fill=ifelse(mean_intensity_changes>0, "pos","neg"), 
             size=abs(mean_intensity_changes))) +
  geom_point(shape=21, color="black") +
  theme_bw()+
  facet_grid(type2~type3, scale="free")

##arange season_year ...
## but bs

#### ratios relative intensities ####

dat %>%
  select(-type) %>%
  group_by(met_name, type2, date) %>%
  spread(type3,max_intensity) %>%
  mutate(ratio=intracellular/extracellular) %>% 
  filter(!is.na(ratio)) %>% 
  filter(is.finite(ratio)) %>%
  ggplot(aes(x=as.Date(date), y = ratio, color=met_name )) +
  geom_point() +
  facet_wrap(~type2, scale="free", ncol=1)




