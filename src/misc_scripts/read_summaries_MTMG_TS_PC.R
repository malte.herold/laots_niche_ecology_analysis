## summarise mapped reads per ReGe
library(data.table)
library(tidyverse)
library(RColorBrewer)
library(here)

# global paths
source(here("conf","config.R"))

##read list of 78 reges
reges <- read_tsv(paste(REPODIR,"aux_datafiles/representative_bins.filtered.tax.tsv", sep="/"), col_names=c("binID"))
##
samples <- read_tsv(paste(REPODIR,"aux_datafiles/sample_list.txt", sep="/"),
                    col_names = c("sample","date"))

### read flagstat files
files.mt <- dir(paste(RESULTSDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"), 
                pattern = "mt.+flagstat.txt",
                full.names=T)
files.mg  <- dir(paste(RESULTSDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"),
                            pattern = "mg.+flagstat.txt",
                            full.names = T)

fstat.mt <- tibble(sample = gsub("([AD]\\d+)\\..+","\\1",basename(files.mt)),type="mt") %>% 
  mutate(data = map(files.mt, function(x) read_tsv(x, col_names=F) %>% t() %>% as.data.frame() ))
                    
ts.fstat.mt <- fstat.mt %>% unnest(data)

colnames(ts.fstat.mt)[3:13] = c("total","duplicates","mapped","paired","read1","read2","properly_paired",
                                "self_mate_mapped","singletons","mate_mapped_diff_chr","mate_mapped_diff_chr_mapq5")

##mg
files.mg <- dir(paste(RESULTSDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"), 
                pattern = "mg.+flagstat.txt",
                full.names=T)
files.mg <- dir(paste(RESULTSDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"),
                            pattern = "mg.+flagstat.txt",
                            full.names = T)

fstat.mg <- tibble(sample = gsub("([AD]\\d+)\\..+","\\1",basename(files.mg)), type="mg") %>% 
  mutate(data = map(files.mg, function(x) read_tsv(x, col_names=F) %>% t() %>% as.data.frame() ))

ts.fstat.mg <- fstat.mg %>% unnest(data)

colnames(ts.fstat.mg)[3:13] = c("total","duplicates","mapped","paired","read1","read2","properly_paired",
                                "self_mate_mapped","singletons","mate_mapped_diff_chr","mate_mapped_diff_chr_mapq5")


### get number of reads per ReGe
## read samtools idxstats files 
## for TIMESERIES

files.mt <- dir(paste(RESULTSDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"), 
                pattern = "mt.+idxstats.txt",
                full.names=T)

data.mt <- tibble(sample = gsub("([AD]\\d+)\\..+","\\1",basename(files.mt)),type="mt") %>% 
  mutate(data = map(files.mt, function(x) read_tsv(x,col_names=c("contig","length","mapped_no","unmapped_no"))))

ts.mt <- data.mt %>% unnest(data)
    
##add binID
ts.mt <- ts.mt %>%
  mutate(binID = gsub("([AD]\\d+_[^_]+)_.+", "\\1", contig))

## group per ReGe filter
reges78.mt <- ts.mt %>% 
  filter(binID %in% reges$binID) %>%
  group_by(sample,type) %>%
  summarise(sum_mapped_reges78 = sum(mapped_no), sum_unmapped_reges78 = sum(unmapped_no))

##mg
files.mg <- dir(paste(RESULTSDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"), 
                pattern = "mg.+idxstats.txt",
                full.names=T)

data.mg <- tibble(sample = gsub("([AD]\\d+)\\..+","\\1",basename(files.mg)),type="mg") %>% 
  mutate(data = map(files.mg, function(x) read_tsv(x,col_names=c("contig","length","mapped_no","unmapped_no"))))

ts.mg <- data.mg %>% unnest(data)

##add binID
ts.mg <- ts.mg %>%
  mutate(binID = gsub("([AD]\\d+_[^_]+)_.+", "\\1", contig))

## group per ReGe filter
reges78.mg <- ts.mg %>% 
  filter(binID %in% reges$binID) %>%
  group_by(sample,type) %>%
  summarise(sum_mapped_reges78 = sum(mapped_no), sum_unmapped_reges78 = sum(unmapped_no))


##combine
ts.fstat.mg %>% select(sample, type, total, mapped) %>%
  left_join(reges78.mg) %>%
  left_join(samples) -> mg.readcounts.ts
ts.fstat.mt %>% select(sample, type, total, mapped) %>%
  left_join(reges78.mt) %>%
  left_join(samples) -> mt.readcounts.ts

totalcounts.ts <- bind_rows(mt.readcounts.ts, mg.readcounts.ts) %>%
  gather(count, reads, -sample, -type, -date)

##plot
totalcounts.ts %>%
  ggplot(aes ( x=date, y=reads, fill=count)) +
    geom_bar(stat="identity", position="dodge") + 
    facet_wrap(~type, ncol=1)


## write out tables
write.table(mt.readcounts.ts, paste(RESULTSDIR,"Output/readcounts_mt_TS.txt",sep="/"), sep="\t", row.names=F, col.names=T, quote=F)
write.table(mg.readcounts.ts, paste(RESULTSDIR,"Output/readcounts_mg_TS.txt",sep="/"), sep="\t", row.names=F, col.names=T, quote=F)


### Pulse chase

samples <- read_tsv(paste(REPODIR,"aux_datafiles/pulse_chase_samples.tsv", sep="/"),
                    col_names = c("timepoint","condition","sample"))

### read flagstat files
files.mt <- dir(paste(PCDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"), 
                pattern = "mt.+flagstat.txt",
                full.names=T)

fstat.mt <- tibble(sample = gsub("(\\d+)\\..+","\\1",basename(files.mt)),type="mt") %>% 
  mutate(data = map(files.mt, function(x) read_tsv(x, col_names=F) %>% t() %>% as.data.frame() ))

ts.fstat.mt <- fstat.mt %>% unnest(data)

colnames(ts.fstat.mt)[3:13] = c("total","duplicates","mapped","paired","read1","read2","properly_paired",
                                "self_mate_mapped","singletons","mate_mapped_diff_chr","mate_mapped_diff_chr_mapq5")

##mg
files.mg  <- dir(paste(PCDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"),
                            pattern = "mg.+flagstat.txt",
                            full.names = T)

fstat.mg <- tibble(sample = gsub("(\\d+)\\..+","\\1",basename(files.mg)), type="mg") %>% 
  mutate(data = map(files.mg, function(x) read_tsv(x, col_names=F) %>% t() %>% as.data.frame() ))

ts.fstat.mg <- fstat.mg %>% unnest(data)

colnames(ts.fstat.mg)[3:13] = c("total","duplicates","mapped","paired","read1","read2","properly_paired",
                                "self_mate_mapped","singletons","mate_mapped_diff_chr","mate_mapped_diff_chr_mapq5")


### get number of reads per ReGe
## read samtools idxstats files 
## for TIMESERIES

files.mt <- dir(paste(PCDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"), 
                pattern = "mt.+idxstats.txt",
                full.names=T)

data.mt <- tibble(sample = gsub("(\\d+)\\..+","\\1",basename(files.mt)),type="mt") %>% 
  mutate(data = map(files.mt, function(x) read_tsv(x,col_names=c("contig","length","mapped_no","unmapped_no"))))

ts.mt <- data.mt %>% unnest(data)

##add binID
ts.mt <- ts.mt %>%
  mutate(binID = gsub("([AD]\\d+_[^_]+)_.+", "\\1", contig))

## group per ReGe filter
reges78.mt <- ts.mt %>% 
  filter(binID %in% reges$binID) %>%
  group_by(sample,type) %>%
  summarise(sum_mapped_reges78 = sum(mapped_no), sum_unmapped_reges78 = sum(unmapped_no))

##mg
files.mg <- dir(paste(PCDIR,"PopulationAnalysis/Calculations/ContigLevel",sep="/"), 
                pattern = "mg.+idxstats.txt",
                full.names=T)

data.mg <- tibble(sample = gsub("(\\d+)\\..+","\\1",basename(files.mg)),type="mg") %>% 
  mutate(data = map(files.mg, function(x) read_tsv(x,col_names=c("contig","length","mapped_no","unmapped_no"))))

ts.mg <- data.mg %>% unnest(data)

##add binID
ts.mg <- ts.mg %>%
  mutate(binID = gsub("(\\d+_[^_]+)_.+", "\\1", contig))

## group per ReGe filter
reges78.mg <- ts.mg %>% 
  filter(binID %in% reges$binID) %>%
  group_by(sample,type) %>%
  summarise(sum_mapped_reges78 = sum(mapped_no), sum_unmapped_reges78 = sum(unmapped_no))


##combine
ts.fstat.mg %>% select(sample, type, total, mapped) %>%
  left_join(reges78.mg) %>%
  left_join(samples) -> mg.readcounts.pc
ts.fstat.mt %>% select(sample, type, total, mapped) %>%
  left_join(reges78.mt) %>%
  left_join(samples) -> mt.readcounts.pc

totalcounts.pc <- bind_rows(mt.readcounts.pc, mg.readcounts.pc) %>%
  gather(count, reads, -sample, -type, -timepoint, -condition)

##plot
totalcounts.pc %>%
  ggplot(aes ( x=timepoint, y=reads, fill=count)) +
  geom_bar(stat="identity", position="dodge") + 
  facet_grid(condition~type)


## write out tables
write.table(mt.readcounts.pc, paste(RESULTSDIR,"Output/readcounts_mt_PC.txt",sep="/"), sep="\t", row.names=F, col.names=T, quote=F)
write.table(mg.readcounts.pc, paste(RESULTSDIR,"Output/readcounts_mg_PC.txt",sep="/"), sep="\t", row.names=F, col.names=T, quote=F)



#### overall numbers
bind_rows(mg.readcounts.ts, mt.readcounts.ts) %>%
  group_by(type) %>%
  summarise(mapped_perc = sum(as.numeric(mapped))/sum(as.numeric(total)),
            mapped_rege78_perc = sum(as.numeric(sum_mapped_reges78))/sum(as.numeric(total)),
            mean_total = mean(total),
            mean_mapped = mean(mapped)
            )


bind_rows(mg.readcounts.pc, mt.readcounts.pc) %>%
  group_by(type) %>%
  summarise(mapped_perc = sum(as.numeric(mapped))/sum(as.numeric(total)),
            mapped_rege78_perc = sum(as.numeric(sum_mapped_reges78))/sum(as.numeric(total)),
            mean_total = mean(total),
            mean_mapped = mean(mapped)
  )



