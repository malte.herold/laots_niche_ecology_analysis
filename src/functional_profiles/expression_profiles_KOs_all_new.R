##expression profiles


library(data.table)
library(tidyverse)
library(vegan)
library(ggplot2)
library(here)

# global paths
source(here("conf","config.R"))

##samplelist
samples=read.table(paste(REPODIR,"aux_datafiles","sample_list.txt",sep="/"))
#get list of representative bins 78
regelist=read.table(paste(REPODIR,"aux_datafiles","representative_bins.filtered.tax.tsv",sep="/"))

## annotation
anno.f <- readRDS(paste(RESULTSDIR,"Output/Reges_annotations_genesfuncstax.RDS", sep="/"))
anno <- anno.f %>% select(binID, FunC_ID, order, family, genus, species) %>% distinct() %>%
  mutate(FunC_ID = as.factor(FunC_ID))

##read in FOAM ontology
# foam <- read_tsv(paste(REPODIR,"aux_datafiles/FOAM-onto_rel1.tsv",sep="/"))

## read in expression data
expr.full <- readRDS(paste(RESULTSDIR,"Output/GeneLevel_Expressiondata_combined_TimeSeries.RDS",sep="/"))

##FOR TESTING subset of bins
binlist = c("D51_G1.1.2", "A01_O1.2.4","D04_G2.5")

## select subset expression values
expr.subs <- expr.full %>%
  filter(binID %in% regelist$V1) %>%
  # filter(binID %in% binlist) %>%
  filter(date > "2011-01-25") %>%
  #filter(date %in% c("2012-01-11","2011-11-16","2011-05-13")) %>%
  select(GeneID, binID, KO, date, MT_depth_scaledmappedreads, MG_depth_scaledmappedreads, MP_spectr_counts)

rm(expr.full)

## reduce to expression profiles based on KO, ReGe

# profiles <- expr.subs %>% 
#   filter(!is.na(KO)) %>%
#   group_by(binID, KO, date) %>%
#   summarise(MT_mean = mean(MT_depth_scaledmappedreads), 
#             MG_mean = mean(MG_depth_scaledmappedreads), 
#             MP_sum = sum(MP_spectr_counts, na.rm=T), 
#             MT_max = max(MT_depth_scaledmappedreads),
#             MG_max = max(MG_depth_scaledmappedreads))

# determine active / inactive by MT/MG ratio MP presence
# profiles.filt <- profiles %>% 
#   filter(MG_mean > 0) %>%
#   mutate(ratio = MT_mean/MG_mean) %>% 
#   mutate(max_ratio = MT_max/MG_max)
# 
# CUTOFF_RATIO = 1
# 
# profiles.filt <- profiles.filt %>% 
#   mutate(Active=ifelse(MP_sum >= 2, 1, ifelse(ratio >= CUTOFF_RATIO, 1, 0)))
# 
# # inspect cases where max_ratio > 1 but inactivte
# profiles.filt %>%
#   filter(Active != 1 ) %>%
#   filter(max_ratio > 1) %>% 
#   group_by(binID, KO) %>%
#   summarise(n = length(KO))
# 
# ##active KOs per bin
# profiles.filt %>%
#   filter(Active==1) %>%
#   group_by(binID, date) %>%
#   tally() %>%
#   left_join(anno) %>%
#   ggplot(aes(x=as.Date(date), y=n, fill = FunC_ID, group=binID)) +
#   geom_bar(stat="identity", color="black")
# 
# profiles.filt2 <- profiles.filt %>% 
#   mutate(Active=ifelse(MP_sum >= 2, 1, ifelse(max_ratio >= CUTOFF_RATIO, 1, 0)))
# profiles.filt2 %>%
#   filter(Active==1) %>%
#   group_by(binID, date) %>%
#   tally() %>%
#   left_join(anno) %>%
#   ggplot(aes(x=as.Date(date), y=n, fill = FunC_ID, group=binID)) + 
#   geom_bar(stat="identity", color="black")
# 
# # inspect MG inflated cases
# profiles.filt %>% filter(ratio>1000) %>% filter(MT_mean > 1) %>% View
# 
# # inspect cases where MG < 1 but Active
# profiles.filt %>% 
#   filter(Active == 1) %>% 
#   filter(MG_mean <1)

## genewise activity ratio
# if MG < 1 replace with MG  = 1
profiles <- expr.subs %>% 
  mutate(MG_depth_cor = ifelse(MG_depth_scaledmappedreads < 1 , 1, MG_depth_scaledmappedreads)) %>%
  mutate(mtmg_ratio = MT_depth_scaledmappedreads / MG_depth_cor) %>%
  mutate(mtmg_ratio = ifelse(!is.finite(mtmg_ratio), NA, mtmg_ratio))
  
## reduce to KO profiles if any gene with KO is "considered active" set to "active"
profiles.filt <- profiles %>%
  filter(!is.na(KO)) %>%
  group_by(date, binID, KO) %>%
  summarise(Active = ifelse(any(MP_spectr_counts >=2, na.rm=T), 1, ifelse(any(mtmg_ratio >= 1, na.rm=T), 1 ,0)))

# pp <- profiles.filt %>%
#   ungroup %>% 
#   select(date,binID,KO,Active) %>%
#   nest(-date) %>%
#   mutate(matrix = map(data, function(x) {
#     yy= x %>% spread(KO, Active) %>% as.data.frame()
#     rownames(yy)=yy$binID
#     # yy[is.na(yy)] <- 0
#     return(yy[-1])
#   }))
# 
# ##calculate distance fit and embedding
# pp <- pp %>%
#   mutate(dist = map(matrix, function(x) {
#     x[is.na(x)] <- 0
#     tt = vegdist(x, method="jaccard",binary=T,na.rm=T)
#     return(tt)
#   }))
# pp <- pp %>%  mutate(fit.mds = map(dist, function(x) {
#     cmdscale(x)
#   })) 
# 
# pp <- pp %>%
#   mutate(embed.mds = map2(date,fit.mds, function(x,y) {
#                        data.frame(x = y[,1], y = y[,2], 
#                        date= x,
#                        binID = rownames(y) ) %>%
#                        left_join(anno) } ))
# 
# 
# pp$embed.mds[1][[1]] %>%
#   ggplot(aes ( x=x, y=y, color= as.factor(FunC_ID))) +
#   geom_point()

## make matrix for distance calculation etc, fill with 0 not with NA if KO not present
kotpmat <- profiles.filt %>%
  ungroup() %>%
  # mutate(bin_tp_profile = paste(binID, date, sep="_")) %>%
  select(binID, date, KO, Active) %>%
  spread(KO, Active, fill=0)
#remove empty rows
kotpmat <- kotpmat[rowSums(kotpmat[,-c(1,2)])!=0,]

# calculate dist, designdist is faster, for testing
distobj <- vegdist(kotpmat[,-c(1,2)], method="jaccard",binary=T,na.rm=T)
distobj <- designdist(kotpmat[,-c(1,2)], method="(A+B-2*J)/(A+B-J)", terms="binary")

# pcoa
fitobj <- cmdscale(distobj, k=2, eig=T)

#summary MG depth for plotting
summary_mg <- profiles  %>%
  group_by(binID, date) %>%
  mutate(mtmg_ratio = ifelse(!is.finite(mtmg_ratio), NA, mtmg_ratio)) %>%
  summarise(mean_MG_depth = mean(MG_depth_scaledmappedreads),
            mean_mtmg_ratio = mean(mtmg_ratio, na.rm=T),
            median_mtmg_ratio = median(mtmg_ratio, na.rm=T))

#df for plotting
embed.mds <- data.frame(x=fitobj$points[,1], y= fitobj$points[,2],
                        binID = kotpmat$binID,
                        date = kotpmat$date) %>%
  # mutate(binID = gsub("(.+_)\\d[2]-\\d[2]-\\d[2]$", "\\1", bin_tp))
  left_join(anno) %>%
  left_join(summary_mg)

var_expl <- round(fitobj$eig*100/sum(fitobj$eig),2)


globtheme <- theme_set( theme_bw()+
                          theme(panel.grid.minor = element_blank(),
                                panel.grid.major = element_blank(),
                                panel.border = element_blank(),
                                axis.line = element_line(colour = "black"),
                                strip.background = element_rect(
                                  color="white", fill="white"),
                                strip.text.x = element_text(
                                  face = "bold"),
                                strip.text.y = element_text(angle=0),
                                text = element_text(size=14),
                                axis.title = element_text(size = 14 ),
                                #title = element_text(face = "bold"),
                                axis.text = element_text(size = 15),
                                legend.title = element_text(size = 15, face="bold"),
                                legend.text = element_text(size=14)
                          ))
update_geom_defaults("point", list(size = 3.5))

# embed.mds <- readRDS(paste(RESULTSDIR, "Output", "figures_rds", "s-fig6_dolphinplot.rds", sep="/"))
# saveRDS(embed.mds, paste(RESULTSDIR, "Output", "figures_rds", "s-fig6_dolphinplot_ver2.rds", sep="/"), version = 2)
# embed.mds <- readRDS(paste(RESULTSDIR, "Output", "figures_rds", "s-fig6_dolphinplot_ver2.rds", sep="/"))
# var_expl = c(6.71, 4.11)
p.dolphin <- embed.mds %>%
 ggplot( aes(x=x, y=y, color = paste0("FunC-",FunC_ID))) + 
  geom_point(aes(size=mean_mtmg_ratio,  alpha=log(mean_MG_depth))) +
  stat_ellipse( aes(group=binID), alpha = 0.8, show.legend = F) +
  scale_color_brewer(palette="Set1") +
  xlab(paste("MDS1,", var_expl[1], "%")) +
  ylab(paste("MDS2,", var_expl[2], "%")) +
  labs(color = "FunC",
       size = "Mean MT/MG ratio",
       alpha = "log(Mean MG depth)")

saveRDS(embed.mds, paste(RESULTSDIR, "Output", "figures_rds", "s-fig6_dolphinplot.rds", sep="/"))
outpdf <- paste(OUTPLOTDIR, "s-fig6_dolphinplot.pdf", sep="/")
pdf(outpdf, height = 8, width = 12)
p.dolphin
dev.off()
