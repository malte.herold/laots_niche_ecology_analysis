# Script collection

This repository is a collection of scripts used in the analysis presented in the manuscript:
`Integration of time-series meta-omics data reveals how microbial ecosystems respond to disturbance`

## dependencies

R libraries used:

```
ampvis2
biobroom
broom
cowplot
data.table
dbscan
DESeq2
dplyr
forcats
ggplot2
ggplus
ggpubr
ggrepel
grid
gridExtra
here
imputeTS
lemon
lubridate
magrittr
pheatmap
phyloseq
psych
RColorBrewer
readxl
readxl
reshape2
scales
showtext
stringr
tidyverse
UpSetR
vegan
```



## configuration file

Most `R` scripts initialise global paths and settings through load a ([config file](conf/config.R)), relative to the repository, with the following statement:
`source(here("conf","config.R"))`

The `RESULTSDIR` variable should point towards the output of the time-series analysis with the following repository:
https://git-r3lab.uni.lu/shaman.narayanasamy/LAO-time-series

## auxiliary datafiles
`aux_datafiles` contains conversion tables used in several scripts, e.g.,
sampleIDs to dates.

## script collection
`src` contains analysis scripts according to the step they were utilised for, e.g. `plotting` contains scripts used to generate the plots for the figures in the manuscript

some scripts are to be run manually, i.e., step-by-step

## data 
all intermediate data files generated with or required as input for the scripts in the repository are available under the following link:

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3590397.svg)](https://doi.org/10.5281/zenodo.3590397)

note that this archive also contains files that were not incorporated in the final analysis

## internal
additional scripts for intermediate analyses or testing can be found in:

https://git-r3lab.uni.lu/malte.herold/Niche_ecology_LAOTS

https://git-r3lab.uni.lu/malte.herold/2019_niche_ecology_lao-ts

